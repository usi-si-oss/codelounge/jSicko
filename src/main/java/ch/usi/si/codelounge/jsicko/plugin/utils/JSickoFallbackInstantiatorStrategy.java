/*
 * Copyright (C) 2022 Andrea Mocci and CodeLounge https://codelounge.si.usi.ch
 *
 * This file is part of jSicko - Java SImple Contract checKer.
 *
 *  jSicko is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 * jSicko is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jSicko.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.usi.si.codelounge.jsicko.plugin.utils;

import java.io.Serializable;
import org.objenesis.instantiator.ObjectInstantiator;
import org.objenesis.strategy.BaseInstantiatorStrategy;
import org.objenesis.strategy.SerializingInstantiatorStrategy;
import org.objenesis.strategy.StdInstantiatorStrategy;

/**
 * JSicko composed fallback strategy for classes without default empty constructor.
 * <p>
 * The composed fallback strategy supports the following cases:
 * <p>
 * If the class is Serializable, it will use Kryo's SerializingInstantiatorStrategy, which (quoting Kryo's
 * documentation) uses Java's built-in serialization mechanism to create an instance. Using this, the class must
 * implement java.io.Serializable and the first zero argument constructor in a super class is invoked.
 * <p>
 * If the class is not Serializable, it will default to StdInstantiatorStrategy, which uses JVM specific APIs to create
 * an instance of a class without calling any constructor at all. This is in general dangerous if there is some logic in
 * the constructors that is expected to be called, but there is no alternative in this case.
 */
public class JSickoFallbackInstantiatorStrategy extends BaseInstantiatorStrategy {

  private static final SerializingInstantiatorStrategy serializingInstantiatorStrategy =
      new SerializingInstantiatorStrategy();
  private static final StdInstantiatorStrategy stdInstantiatorStrategy =
      new StdInstantiatorStrategy();

  @Override
  public <T> ObjectInstantiator<T> newInstantiatorOf(Class<T> type) {
    if (Serializable.class.isAssignableFrom(type)) {
      return serializingInstantiatorStrategy.newInstantiatorOf(type);
    }
    return stdInstantiatorStrategy.newInstantiatorOf(type);
  }
}
